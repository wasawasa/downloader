import re

def escape_space(string):
    tmp = ''
    for c in string:
        if ' ' in c:
            tmp += "_"
        else:
            tmp += c
    return tmp

def escape_fwd_slash(string):
    tmp = ''
    for c in string:
        if '/' in c:
            tmp += '=-='
        else:
            tmp += c
    return tmp

def get_first_number_from_text(string):
    for i in range(0, len(string)):
        if string[i].isdigit():
            tmp = ''
            for j in range(i, len(string)):
                if string[j].isdigit():
                    tmp += string[j]
                elif ',' not in string[j]:
                    break
            return tmp

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def timeout_handler(signum, frame):
    raise RuntimeError('Execution timeout')

# print(get_first_number_from_text(u"About 37,800,000 results (0.33 seconds) "))
