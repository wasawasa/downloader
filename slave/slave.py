import os
import pika
import ujson
import signal

import sys
sys.path.append('workers')
import logging
from logging.handlers import RotatingFileHandler
import threading
import time
import argparse

### The following bundle works on fetching image urls' relating to keywords
# from worker_lord_naive import NaiveWorkerLord
# from worker_keyword_imageurl import KeywordImageUrlWorker
# from worker_convey_single_file import SingleFileConveyWorker

### The following bundle works on downloading images from a url list.
### e.g. Flickr images, quanjing images, keyword images
# from worker_lord_gapconvey import GapConveyWorkerLord
# from worker_pic_download import PicDownloadWorker
# from worker_convey_zip_folder import ZipFolderConveyWorker

### The following bundle works on fetching HTML elements on webpages.
### e.g. Chinese Wikipedia data
# from worker_lord_gapconvey import GapConveyWorkerLord
# from worker_tieba_data import TiebaDataFetcher
# from worker_convey_zip_folder import ZipFolderConveyWorker

### The following bundle works on getting number of results given keywords in search engines
# from worker_lord_naive import NaiveWorkerLord
# from worker_keyword_fields import KeywordFieldsWorker
# from worker_convey_single_file import SingleFileConveyWorker

### The following bundle works on weibo tasks
from worker_lord_gapconvey import GapConveyWorkerLord
from worker_pure_html import PureHtmlWorker
from worker_convey_zip_folder import ZipFolderConveyWorker


def timeout_handler(signum, frame):
    raise RuntimeError('Execution timeout')


class Slave:
    def __init__(self, host, slave_ip, screen, s3_bucket,
                 s3_folder, num_packup, max_download_sec, awake_frequency, port=5672):
        self.slave_ip = slave_ip
        self.port = port
        self.screen = screen
        self.s3_bucket = s3_bucket
        self.s3_folder = s3_folder
        self.num_packup = num_packup
        self.host = host
        self.max_download_sec = max_download_sec
        self.frequency = awake_frequency
        self.is_started = False

        self.worker_lord = GapConveyWorkerLord(
            PureHtmlWorker(),
            ZipFolderConveyWorker(s3_dest_bucket=s3_bucket, s3_dest_folder=s3_folder),
            self.s3_bucket,
            self.s3_folder,
            self.num_packup
        )

        self.task_connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self.host, port=self.port))
        self.task_channel = self.task_connection.channel()
        self.task_channel.queue_declare(queue='task', durable=True)
        self.task_channel.basic_qos(prefetch_count=1)
        self.downloadertag = self.task_channel.basic_consume(self.callback, queue='task')

        self.task_channel.exchange_declare(exchange='broadcast',
                                           type='fanout')
        self.task_channel.queue_bind(exchange='broadcast',
                                     queue='task')

        self.message_connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self.host, port=self.port))
        self.message_channel = self.message_connection.channel()
        self.message_channel.queue_declare(queue='message')

        self.beating = True
        self.heart_beat_handler = threading.Thread(target=self.send_heart_beat)
        self.heart_beat_handler.start()

        logging.info(' [*] Waiting for messages. To exit press CTRL+C')
        try:
            self.is_started = True
            self.task_channel.start_consuming()
        except KeyboardInterrupt:
            logging.info('KeyboardInterrupt. Stop.')
            self.stop()
        except RuntimeError:
            logging.info('RuntimeError. Stop.')
            self.stop()

    def send_heart_beat(self):
        while self.beating:
            reply = {'host': self.slave_ip, 'screen': self.screen}
            self.message_channel.basic_publish(exchange='',
                                          routing_key='message',
                                          body='AWAKE ' + ujson.dumps(reply))
            time.sleep(self.frequency)

    def callback(self, ch, method, properties, body):
        logging.info("Received %r" % (body,))
        if type(body) is bytes:
            body = body.decode('utf-8')

        command = body[:body.find(' ')]
        info = body[body.find(' ') + 1:]
        if command == 'WORK':
            task_string = ujson.loads(info)['task']
            task = ujson.loads(task_string)
            file_name, error = self.handle_work(task)
            ch.basic_ack(delivery_tag=method.delivery_tag)
        elif command == 'STOP':
            # ch.basic_ack(delivery_tag=method.delivery_tag)
            logging.info('Received stop')
            self.stop()
        else:
            logging.warning("Unknown command %r" % (body,))

    def handle_work(self, task):
        error = ''
        try:
            signal.signal(signal.SIGALRM, timeout_handler)
            signal.alarm(self.max_download_sec)
            filename = self.worker_lord.run(task)
            signal.alarm(0)
        except RuntimeError as e:
            if 'timeout' in str(e):
                logging.error('Timeout')
            else:
                logging.info(e)
            filename = None
            error = 'timeout'
        # except Exception as e:
        #     logging.info(e)
        #     filename = None
        #     error = e
        return filename, error

    def stop(self):
        logging.info('stop')
        if not self.is_started:
            logging.info('I want to stop but I am already stopped.')
            return
        signal.alarm(0)
        self.worker_lord.close()
        self.beating = False
        self.heart_beat_handler.join()
        logging.info('Closing slave connection')
        self.task_connection.close()
        self.message_connection.close()
        logging.info('Slave connection closed')
        os.system('screen -S ' + self.screen + ' -X quit')

def set_logger(tag):
    fmt = '%(asctime)s %(levelname)-8s %(filename)s:%(lineno)-3s %(funcName)10s(): %(message)s'
    dfmt = '%m-%d %H:%M:%S'
    logging.basicConfig(level=logging.INFO,
                        format=fmt,
                        datefmt=dfmt,
                        filename='downloader_queue_{}.log'.format(tag),
                        filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt=fmt, datefmt=dfmt)
    console.setFormatter(formatter)
    rotate_handler = RotatingFileHandler('downloader_queue_{}.log'.format(tag), mode='w', maxBytes=5*1024*1024,
                                         backupCount=2, encoding=None, delay=0)
    logging.getLogger('').addHandler(rotate_handler)
    logging.getLogger('').addHandler(console)

if __name__ == '__main__':
    arg_tuple_map = [
        ('host', str, 'master host ip'),
        ('slave', str, 'ip address of this slave client'),
        ('screen', str, 'screen name'),
        ('s3_bucket', str, 's3 bucket'),
        ('s3_folder', str, 's3 folder'),
        ('num_packup', int, 'number of tasks for each zipping up and conveying to s3'),
        ('max_download_sec', int, 'if the execution time, in seconds, of a task exceeds this number, skip the task.'),
        ('awake_frequency', int, 'for which every number of seconds will the slave send AWAKE to master')
    ]
    parse = argparse.ArgumentParser()

    for arg_tuple in arg_tuple_map:
        parse.add_argument(arg_tuple[0], type=arg_tuple[1], help=arg_tuple[2])
    parse.add_argument('--port',
                       type=int,
                       default=5672,
                       help='port number')
    args = parse.parse_args()
    host = args.host
    slave_ip = args.slave
    screen = args.screen
    s3_bucket = args.s3_bucket
    s3_folder = args.s3_folder
    num_packup = args.num_packup
    max_download_sec = args.max_download_sec
    awake_frequency = args.awake_frequency
    port = args.port

    set_logger(screen)

    downloader = Slave(host=host, slave_ip=slave_ip, screen=screen,
                            s3_bucket=s3_bucket, s3_folder=s3_folder,
                            num_packup=num_packup, max_download_sec=max_download_sec,
                            awake_frequency=awake_frequency, port=port)
