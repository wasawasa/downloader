import logging
from worker_convey import ConveyWorker

class ZipFolderConveyWorker(ConveyWorker):

    def work(self, path):
        if not path:
            logging.info('No convey path is set. Stop conveying.')
            return
        logging.info('Packing up files')
        filename = self.pack_files(path)
        logging.info('Conveying')
        self.convey(filename)
        logging.info('Conveyed. Deleting files ' + str(path) + ' and ' + filename)
        self.delete_file(filename)
        self.delete_folder(path)
        logging.info('Files deleted. Next folder is ' + self.get_convey_path())
