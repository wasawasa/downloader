#!/usr/bin/env python
# encoding: utf8
# -*- coding: encoding -*-

# Author: Nan Chen
# Date: Jun 9

import os
from time import sleep
import logging
import abc
import shutil
import zipfile

class ConveyWorker:
    __metaclass__ = abc.ABCMeta

    def __init__(self, s3_dest_bucket, s3_dest_folder):
        self.convey_path = ''
        self.s3_dest_bucket = s3_dest_bucket
        self.s3_dest_folder = s3_dest_folder
        pass

    def set_convey_path(self, new_convey_path):
        self.convey_path = new_convey_path

    def get_convey_path(self):
        return self.convey_path

    def delete_file(self, file_name):
        os.remove(file_name)

    def delete_folder(self, folder_name):
        shutil.rmtree(folder_name)

    def pack_files(self, path):
        zip_file_name = path + '.zip'
        zip_file = zipfile.ZipFile(zip_file_name, 'w')
        for root, dirs, files in os.walk(path):
            for f in files:
                zip_file.write(os.path.join(root, f))
        zip_file.close()
        return zip_file_name

    def convey(self, file_name, mode=None):
        if mode is 'test':
            logging.info('Conveying...')
            sleep(5)
            logging.info('Conveyance done.')
        else:
            os.system('aws s3 cp ' + file_name + ' s3://' +
                      self.s3_dest_bucket + '/' + self.s3_dest_folder + '/')

    @abc.abstractmethod
    def work(self, path):
        pass
