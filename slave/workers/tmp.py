import os
import subprocess

def main():
    num_workers = 10
    task_file_path = '/Users/a67/Tmp/test/url_list.txt'
    save_path = '/Users/a67/Tmp/test/urls'
    with open(task_file_path, 'r') as f:
        num_tasks = len([line for line in f])
    num_each_chunk = num_tasks / num_workers
    for i in range(0, num_workers):
        start_i = i * num_each_chunk
        end_i = (i + 1) * num_each_chunk
        # os.system('python worker_pic_download.py {0} {1} {2} {3}'.format(
        #           str(start_i), str(end_i), task_file_path, save_path))
        subprocess.Popen(['python worker_pic_download.py {0} {1} {2} {3}'.format(
                  str(start_i), str(end_i), task_file_path, save_path)], shell=True)

main()
