#!/usr/bin/env python

# Author: Nan Chen
# Date: Jun 12

import contextlib
import httplib
import os
import ujson
from worker_fetch import FetchWorker
import sys
import logging
import hashlib
from time import sleep
import socket
from selenium import webdriver
from selenium.common import exceptions
from pyvirtualdisplay import Display
from urllib2 import urlopen
import signal
sys.path.append('..')
import cogtu_misc


NUM_RETRY_FETCH = 3

class WeiboParseWorker(FetchWorker):
    def __init__(self):
        FetchWorker.__init__(self)
        self.driver = None
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()

    def get_driver(self):
        if not self.driver:
            self.driver = webdriver.Chrome()
        return self.driver

    def _fetch_data(self, url):
        driver = self.get_driver()
        m = hashlib.md5()
        m.update(url)
        file_id = m.hexdigest()
        acceptable_imgs = []
        wait_length = 0.5
        for i in range(0, NUM_RETRY_FETCH):
            try:
                driver.get(url)
                sleep(wait_length)
                img_tags = driver.find_elements_by_xpath('//img')
                for img_tag in img_tags:
                    img_src = img_tag.get_attribute('src')
                    if not img_src:
                        continue
                    img_src = img_src.encode('utf-8')
                    img_width = img_tag.value_of_css_property('width')
                    img_height = img_tag.value_of_css_property('height')
                    if img_width:
                        img_width = img_width.encode('utf-8')
                    if img_height:
                        img_height = img_height.encode('utf-8')
                    acceptable_imgs.append([img_src, url, img_width, img_height])
            except KeyboardInterrupt:
                raise KeyboardInterrupt
            except exceptions.NoSuchElementException as e:
                logging.warning('Exception ' + str(e) + ' + at ' + str(url))
            except exceptions.StaleElementReferenceException as e:
                logging.warning('Exception ' + str(e) + ' + at ' + str(url))
                wait_length += 0.5
            except httplib.CannotSendRequest as e:
                logging.warning('Exception ' + str(e) + ' + at ' + str(url))
                wait_length += 0.5
            except exceptions.UnexpectedAlertPresentException as e:
                logging.warning('Exception ' + str(e) + ' + at ' + str(url))
                wait_length += 0.5
            if acceptable_imgs:
                break
            logging.info('Retrying ' + str(url) + '...')
            sleep(0.5)
        if not acceptable_imgs:
            return None
        else:
            return file_id, acceptable_imgs

    def write_data_to_file(self, data, filename):
        print filename
        with open(filename, 'w') as f:
            for pair in data:
                print pair[0]
                print pair[1]
                print pair[2]
                print pair[3]
                f.write(pair[0] + '\t' + pair[1] + '\t' + pair[2] + '\t' + pair[3] + '\n')

    def work(self, task):
        url = task['url'].encode('utf-8')
        data_pair = self._fetch_data(url)
        if data_pair is None:
            return None
        else:
            file_id = data_pair[0]
            data = data_pair[1]
            file_id = self.get_save_path() + '/' + file_id
            self.write_data_to_file(data=data, filename=file_id)
            return file_id

    def close(self):
        self.display.stop()

def test_work():
    worker = WeiboParseWorker()
    worker.set_save_path('/Users/a67/Tmp/test')
    tasks = [
        {'url': 'http://tech.sina.com.cn/mobile/mob_brand/#&brand=\u8428\u57fa\u59c6&sort=1'},
        {'url': 'http://city.sina.com.cn/'},
        {'url': 'http://city.sina.com.cn/focus/n/list.html'},
        {'url': 'http://city.sina.com.cn/focus/t/2014-12-12/095947595.html'}
    ]
    for task in tasks:
        worker.work(task)
    print 'Done'

def work_locally(start_i, end_i, task_file_path, save_path):
    worker = WeiboParseWorker()
    worker.set_save_path(save_path)
    with open(task_file_path, 'r') as f:
        for i, line in enumerate(f):
            if i < start_i or i >= end_i:
                continue
            task = ujson.loads(line.strip())
            url = task['url']

            m = hashlib.md5()
            m.update(url)
            file_id = m.hexdigest()
            if os.path.isfile(save_path + '/' + file_id):
                continue

            signal.signal(signal.SIGALRM, cogtu_misc.timeout_handler)
            signal.alarm(30)
            worker.work(task)
            signal.alarm(0)
            if i % 100 == 0:
                print i

# test_work()
# work_locally(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
# work_locally(0, 10, '/Users/a67/Tmp/test/recent_list.txt', '/Users/a67/Tmp/test/urls')
