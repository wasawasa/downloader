#!/usr/bin/env python

# Author: Nan Chen
# Date: Jun 12

import mechanize
import contextlib
import urllib2
import lxml.html
import ujson
from worker_fetch import FetchWorker
import sys
import logging
sys.path.append('..')
import cogtu_misc


NUM_RETRY_FETCH = 3

class WikiDataFetcher(FetchWorker):

    def _get_first_paragraph_text(self, doc, body_text_html):
        p_tag_html = body_text_html[0].xpath('p')
        first_p_text = ''
        for tag in p_tag_html:
            first_p_text = tag.text_content().strip()
            if first_p_text is not '':
                break
        return first_p_text

    def _fetch_data(self, url):
        # url = url.decode('utf-8')
        # if url[:5] == 'http:':
        #     url = 'https' + url[4:]
        # url = url.encode('utf-8')
        url_title = url.split('/')[-1]
        data = dict()
        data['fetch_error'] = []
        try:
            with contextlib.closing(urllib2.urlopen(url)) as page_source:
                page_content = page_source.read()
                doc = lxml.html.document_fromstring(page_content)

                # Get body content html
                body_text_html = doc.xpath('//div[contains(@id, "mw-content-text")]')
                if len(body_text_html) == 0:
                    data['fetch_error'].append('NoBodyContent')
                    return url_title, data

                # Get first paragraph text
                first_paragraph_text = self._get_first_paragraph_text(doc, body_text_html)
                if not first_paragraph_text:
                    data['fetch_error'].append('NoFirstParagraph')

                # Get entry name
                entry_name_html = doc.xpath('//h1[contains(@class, "firstHeading")]')
                if len(entry_name_html) == 0:
                    data['fetch_error'].append('NoEntryName')
                    entry_name = url_title
                else:
                    entry_name = entry_name_html[0].text_content().strip()
        except urllib2.HTTPError:
            logging.info('urllib2.HTTPError. Skip.')
            return url_title, data
        except urllib2.URLError:
            logging.info('urllib2.URLError. Skip.')
            return url_title, data

        data['entry_name'] = entry_name
        data['url'] = url
        data['first_paragraph_text'] = first_paragraph_text
        data['page_content'] = page_content
        return entry_name, data

    def write_data_to_file(self, data, filename):
        print filename
        with open(filename, 'w') as f:
            dum = ujson.dumps(data)
            f.write(dum)

    def work(self, url):
        entry_name, data = self._fetch_data(url)
        entry_name = cogtu_misc.escape_fwd_slash(entry_name)
        filename = self.get_save_path() + '/' + entry_name
        self.write_data_to_file(data=data, filename=filename)
        return filename
