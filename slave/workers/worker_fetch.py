#!/usr/bin/env python
# encoding: utf8
# -*- coding: encoding -*-

# Author: Nan Chen
# Date: Jun 9

import abc

class FetchWorker:
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.save_path = None
        self.task_url = None
        self.browser = None

    def set_save_path(self, new_save_path):
        self.save_path = new_save_path

    def get_save_path(self):
        return self.save_path

    @abc.abstractmethod
    def write_data_to_file(self, data, filename):
        pass

    @abc.abstractmethod
    def work(self, task):
        pass

    @abc.abstractmethod
    def close(self):
        pass