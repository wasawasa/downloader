#!/usr/bin/env python

# Author: Nan Chen
# Date: Jun 12

import mechanize
import contextlib
import urllib2
import httplib
import sys
if sys.version_info[0] < 3:
    from urllib2 import urlopen
else:
    from urllib2.request import urlopen
import lxml.html
import ujson
from worker_fetch import FetchWorker
import sys
import logging
import hashlib
from time import sleep
import socket
sys.path.append('..')
import cogtu_misc


NUM_RETRY_FETCH = 3

class WeiboDateWorker(FetchWorker):

    def _fetch_data(self, url):
        m = hashlib.md5()
        m.update(url)
        file_id = m.hexdigest()
        date_data = []
        for i in range(0, NUM_RETRY_FETCH):
            try:
                with contextlib.closing(urlopen(url)) as page_source:
                    info = page_source.info()
                last_modified_date = info.getdate('last-modified')
                header_date = info.getheader('date')
                date_data = [url, last_modified_date, header_date]
            except KeyboardInterrupt:
                raise KeyboardInterrupt
            except urllib2.HTTPError:
                logging.warning('urllib2.HTTPError at ' + str(url))
                break
            except urllib2.URLError:
                logging.warning('urllib2.URLError at ' + str(url))
                break
            except socket.error:
                logging.warning('socket.error')
                break
            except httplib.IncompleteRead:
                logging.warning('Incomplete read')
            # except Exception as e:
            #     logging.warning('Exception ' + str(e) + ' + at ' + str(url))
            #     html_content = ''
            if date_data:
                break
            logging.info('Retrying ' + str(url) + '...')
            sleep(0.5)
        if not date_data:
            return None
        else:
            return file_id, date_data

    def write_data_to_file(self, data, filename):
        print filename
        with open(filename, 'w') as f:
            f.write(data[0] + '\t')
            if type(data[1]) is tuple:
                for item in data[1]:
                    f.write(str(item) + ' ')
                f.write('\t')
            else:
                f.write('None\t')
            f.write(data[2] + '\n')

    def work(self, task):
        url = task['url'].encode('utf-8')
        data_pair = self._fetch_data(url)
        print url
        print data_pair
        if data_pair is None:
            return None
        else:
            file_id = data_pair[0]
            data = data_pair[1]
            file_id = self.get_save_path() + '/' + file_id
            self.write_data_to_file(data=data, filename=file_id)
            return file_id

def test_work():
    worker = WeiboDateWorker()
    worker.set_save_path('/Users/a67/Tmp/test')
    tasks = [
        {'url': 'http://video.sina.com.cn/p/ent/y/h/2015-05-19/090664963487.html'},
        {'url': 'http://news.sina.com.cn/o/2015-04-17/063931728088.shtml'},
        {'url': 'http://city.sina.com.cn/focus/t/2014-12-12/095947595.html'}
    ]
    for task in tasks:
        worker.work(task)
    print 'Done'

# test_work()
