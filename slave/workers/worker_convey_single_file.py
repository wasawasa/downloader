
from worker_convey import ConveyWorker

class SingleFileConveyWorker(ConveyWorker):
    def work(self, file_name):
        self.convey(file_name)
        self.delete_file(file_name)


