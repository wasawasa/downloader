# Author: Nan Chen
# Date: Jun 9, 2015
import abc
import hashlib
import os
import logging

class WorkerLord:
    __metaclass__ = abc.ABCMeta

    #
    # Name: __init__
    # Params:
    #   procedure: A function pointer. The function defines how this worker lord
    #            should control its worker's behaviour.
    #   fetch_worker: The worker that does single crawling or downloading jobs
    #   convey_worker: The worker that zips files and send the zip to s3
    #   num_task_packup: For every <num_task_packup>, the convey worker should
    #            zip up the done data (written in files) and send to s3
    # Return: Nothing
    # Desc: Initialize a WorkerLord
    #
    def __init__(self, s3_bucket, s3_folder):
        self.task_counter = 0 # Counts number of tasks given out to worker
        self.s3_bucket = s3_bucket
        self.s3_folder = s3_folder

    def increment_task_counter(self):
        self.task_counter += 1

    def get_task_counter(self):
        return self.task_counter

    @staticmethod
    def generate_unique_folder(unique_fname_source):
        logging.info('Setting new directory')
        m = hashlib.md5()
        m.update(unique_fname_source)
        folder_name = m.hexdigest()
        if not os.path.exists(folder_name):
            logging.info('Directory not exist. Making directory ' + str(folder_name))
            os.makedirs(folder_name)
        return folder_name

    @abc.abstractmethod
    def close(self):
        pass

    # This function runs the procedure once.
    @abc.abstractmethod
    def run(self, task):
        pass