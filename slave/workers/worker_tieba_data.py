#!/usr/bin/env python
# encoding: utf8
# -*- coding: encoding -*-

# Author: Nan Chen
# Date: Jun 12

import mechanize
import contextlib
import urllib2
import lxml.html
import ujson
from worker_fetch import FetchWorker
import sys
import logging
sys.path.append('..')
import cogtu_misc
from bs4 import UnicodeDammit

NUM_RETRY_FETCH = 3

class TiebaDataFetcher(FetchWorker):

    def _fetch_data(self, entry_name, url):
        # url = url.decode('utf-8')
        # if url[:5] == 'http:':
        #     url = 'https' + url[4:]
        # url = url.encode('utf-8')
        original_entry_name = entry_name
        data = dict()
        try:
            with contextlib.closing(urllib2.urlopen(url.encode('utf-8'))) as page_source:
                page_content = page_source.read()
            doc = UnicodeDammit(page_content, is_html=True)
            parser = lxml.html.HTMLParser(encoding=doc.original_encoding)
            doc = lxml.html.document_fromstring(page_content, parser=parser)

            bar_name = doc.xpath('//a[contains(@class, "star_title_h3")]')
            if not bar_name:
                bar_name = doc.xpath('//a[contains(@class, "card_title_fname")]')
            if type(bar_name) is list and len(bar_name) > 0:
                entry_name = bar_name[0].text_content().strip()
            num_visits = doc.xpath('//span[contains(@class, "j_visit_num")]')
            if not num_visits:
                num_visits = doc.xpath('//span[contains(@class, "card_menNum")]')
            num_posts = doc.xpath('//span[contains(@class, "j_post_num")]')
            if not num_posts:
                num_posts = doc.xpath('//span[contains(@class, "card_infoNum")]')
            if type(num_visits) is list and len(num_visits) > 0:
                num_visits = num_visits[0].text_content()
                num_visits = cogtu_misc.get_first_number_from_text(num_visits)
            else:
                num_visits = 0
            if type(num_posts) is list and len(num_posts) > 0:
                num_posts = num_posts[0].text_content()
                num_posts = cogtu_misc.get_first_number_from_text(num_posts)
            else:
                num_posts = 0
            num_groups = doc.xpath("//a[contains(@class, 'star_nav_ico_group')]/span")
            if type(num_groups) is list and len(num_groups) > 0:
                num_groups = num_groups[0].text_content()
                num_groups = cogtu_misc.get_first_number_from_text(num_groups)
            else:
                num_groups = 0
        except urllib2.HTTPError:
            logging.info('urllib2.HTTPError. Skip.')
            return None, None
        except urllib2.URLError:
            logging.info('urllib2.URLError. Skip.')
            return None, None

        data['num_visits'] = int(num_visits)
        data['num_posts'] = int(num_posts)
        data['num_groups'] = int(num_groups)
        data['entry_name'] = entry_name
        data['original_entry_name'] = original_entry_name
        data['url'] = url
        return entry_name, data

    def write_data_to_file(self, data, filename):
        with open(filename, 'w') as f:
            dum = ujson.dumps(data)
            f.write(dum)

    def work(self, task):
        entry_name = task.split('-=-=-')[0]
        url = task.split('-=-=-')[1]
        entry_name, data = self._fetch_data(entry_name, url)
        if not data:
            return None
        entry_name = cogtu_misc.escape_fwd_slash(entry_name)
        filename = self.get_save_path() + '/' + entry_name
        self.write_data_to_file(data=data, filename=filename)
        return filename

def test():
    worker = TiebaDataFetcher()
    worker.set_save_path('/Users/a67/Desktop/tmp/test2')
    print worker.work(u"周杰伦-=-=-http://tieba.baidu.com/f?kw=周杰伦")
    print worker.work(u"周 杰伦-=-=-http://tieba.baidu.com/f?kw=周杰伦")
    print worker.work(u"杨乐乐-=-=-http://tieba.baidu.com/f?kw=%D1%EE%C0%D6%C0%D6")
    print worker.work(u"蝴蝶姐姐-=-=-http://tieba.baidu.com/f?kw=%BA%FB%B5%FB%BD%E3%BD%E3")
    print worker.work(u"唐妈妈-=-=-http://tieba.baidu.com/f?kw=%BA%FB%B5%FB%BD%E3%BD%E3")
    print worker.work(u"唐嫣-=-=-http://tieba.baidu.com/f?kw=%BA%FB%B5%FB%BD%E3%BD%E3")

# test()
