from worker_lord import WorkerLord
import logging

class NaiveWorkerLord(WorkerLord):
    def __init__(self, fetch_worker, convey_worker, s3_bucket, s3_folder):
        WorkerLord.__init__(self, s3_bucket, s3_folder)
        self.convey_worker = convey_worker
        self.fetch_worker = fetch_worker

    def run(self, task):
        unique_filename = self.fetch_worker.work(task)
        self.convey_worker.work(unique_filename)
        return unique_filename

    def close(self):
        pass
