#!/usr/bin/env python

# Author: Nan Chen
# Date: Jun 12

import mechanize
import contextlib
import urllib2
import httplib
import sys
if sys.version_info[0] < 3:
    from urllib2 import urlopen
else:
    from urllib2.request import urlopen
import ujson
from worker_fetch import FetchWorker
import sys
import logging
import hashlib
from time import sleep
import socket
from bs4 import UnicodeDammit
sys.path.append('..')
import cogtu_misc


NUM_RETRY_FETCH = 3

class PureHtmlWorker(FetchWorker):

    def _fetch_data(self, url):
        m = hashlib.md5()
        m.update(url)
        file_id = m.hexdigest()
        html_content = ''
        for i in range(0, NUM_RETRY_FETCH):
            try:
                with contextlib.closing(urlopen(url)) as page_source:
                    html_content = page_source.read()
            except KeyboardInterrupt:
                raise KeyboardInterrupt
            except urllib2.HTTPError:
                logging.warning('urllib2.HTTPError at ' + str(url))
                break
            except urllib2.URLError:
                logging.warning('urllib2.URLError at ' + str(url))
                break
            except socket.error:
                logging.warning('socket.error')
                break
            except httplib.IncompleteRead:
                logging.warning('Incomplete read')
            except Exception as e:
                logging.warning('Exception ' + str(e) + ' + at ' + str(url))
            if html_content:
                break
            logging.info('Retrying ' + str(url) + '...')
            sleep(0.5)
        if not html_content:
            return None
        else:
            return file_id, html_content

    def write_data_to_file(self, data, filename):
        print filename
        with open(filename, 'w') as f:
            f.write(data)

    def work(self, task):
        url = task['url'].encode('utf-8')
        data_pair = self._fetch_data(url)
        if data_pair is None:
            return None
        else:
            file_id = data_pair[0]
            data = data_pair[1]
            file_id = self.get_save_path() + '/' + file_id
            self.write_data_to_file(data=data, filename=file_id)
            return file_id

    def close(self):
        pass

def test_work():
    worker = PureHtmlWorker()
    worker.set_save_path('/Users/a67/Tmp/test')
    tasks = [
        {'url': 'http://city.sina.com.cn/'},
        {'url': 'http://city.sina.com.cn/focus/n/list.html'},
        {'url': 'http://city.sina.com.cn/focus/t/2014-12-12/095947595.html'},
        {'url': 'http://games.sina.com.cn/o/z/wuxia/2015-07-10/fxewnia8859642.shtml'},
        {'url': 'http://edu.sina.com.cn/l/11187.html'}
    ]
    for task in tasks:
        worker.work(task)
    print 'Done'

# test_work()
