#!/usr/bin/env python
# encoding: utf8
# -*- coding: encoding -*-

# Author: Nan Chen
# Date: Jun 9
import sys
import urllib2
import ujson
import signal
sys.path.append('../..')
import cogtu_misc
import os
if sys.version_info[0] < 3:
    from urllib2 import urlopen
else:
    from urllib2.request import urlopen
from time import sleep
from worker_fetch import FetchWorker
import contextlib
import logging
import hashlib
import socket
NUM_RETRY_FETCH = 3

class PicDownloadWorker(FetchWorker):

    def _fetch_data(self, url):
        m = hashlib.md5()
        m.update(url)
        img_id = m.hexdigest()
        postfix = url.split('.')[-1]
        if len(postfix) > 6:
            postfix = 'jpg'
        img_id += '.' + postfix
        img_data = ''
        for i in range(0, NUM_RETRY_FETCH):
            try:
                with contextlib.closing(urlopen(url)) as page_source:
                    img_data = page_source.read()
            except KeyboardInterrupt:
                raise KeyboardInterrupt
            except urllib2.HTTPError:
                logging.warning('urllib2.HTTPError at ' + str(url))
                break
            except urllib2.URLError:
                logging.warning('urllib2.URLError at ' + str(url))
                break
            except socket.error:
                logging.warning('socket.error')
                break
            # except Exception as e:
            #     logging.warning('Exception ' + str(e) + ' + at ' + str(url))
            #     img_data = ''
            if img_data.strip() is not '':
                break
            logging.info('Retrying ' + str(url) + '...')
            sleep(0.5)
        if img_data.strip() is '':
            return None
        else:
            return img_id, img_data

    def write_data_to_file(self, data, filename):
        with open(filename, 'w') as f:
            f.write(data)

    def work(self, task):
        url = task['url']
        data_pair = self._fetch_data(url)
        if data_pair is None:
            return None
        else:
            filename = self.get_save_path() + '/' + data_pair[0]
            self.write_data_to_file(data=data_pair[1], filename=filename)
            return filename

    def close(self):
        pass

def test_work():
    worker = PicDownloadWorker()
    worker.set_save_path('/Users/a67/Tmp/test')
    tasks = [
        {'url': 'http://tech.sina.com.cn/mobile/mob_brand/#&brand=\u8428\u57fa\u59c6&sort=1'},
        {'url': 'http://city.sina.com.cn/'},
        {'url': 'http://city.sina.com.cn/focus/n/list.html'},
        {'url': 'http://city.sina.com.cn/focus/t/2014-12-12/095947595.html'}
    ]
    for task in tasks:
        worker.work(task)
    print 'Done'

def work_locally(start_i, end_i, task_file_path, save_path):
    worker = PicDownloadWorker()
    worker.set_save_path(save_path)
    with open(task_file_path, 'r') as f:
        for i, line in enumerate(f):
            if i < start_i or i >= end_i:
                continue
            task = ujson.loads(line.strip())
            url = task['url']

            m = hashlib.md5()
            m.update(url)
            file_id = m.hexdigest()
            if os.path.isfile(save_path + '/' + file_id):
                continue

            signal.signal(signal.SIGALRM, cogtu_misc.timeout_handler)
            signal.alarm(30)
            worker.work(task)
            signal.alarm(0)
            if i % 100 == 0:
                print i

# work_locally(0, 10, '/Users/a67/Tmp/test/url_list.txt', '/Users/a67/Tmp/test/urls')
# work_locally(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
