#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-

from GoogleScraper import scrape_with_config, GoogleSearchError
from worker_fetch import FetchWorker
import hashlib
import logging
import sys
import math
sys.path.append('..')
import cogtu_misc


class KeywordImageUrlWorker(FetchWorker):

    def __init__(self, search_engines):
        FetchWorker.__init__(self)
        self.gscraper_config = {
            'SCRAPING': {
                'search_engines': search_engines,
                'search_type': 'image',
                'scrape_method': 'selenium',
            },
            'GLOBAL': {
                'do_caching': 'False'
            }
        }

    def get_num_pages_from_num_urls(self, num_urls):
        return math.ceil(float(num_urls) / 100.0)

    def fetch_image_urls(self, keyword, num_urls):
        self.gscraper_config['SCRAPING']['keyword'] = keyword
        self.gscraper_config['SCRAPING']['num_pages_for_keyword'] =\
            self.get_num_pages_from_num_urls(num_urls)
        try:
            search = scrape_with_config(self.gscraper_config)
        except GoogleSearchError as e:
            logging.info(e)
            search = ''
            return
        image_urls = list()
        for serp in search.serps:
            image_urls.extend([link.link for link in serp.links])
        if num_urls > len(image_urls):
            return image_urls
        else:
            return image_urls[:num_urls]

    def write_data_to_file(self, image_url, filename):
        with open(filename, 'w') as f:
            for url in image_url:
                f.write(url + '\n')

    def work(self, task):
        if not task:
            logging.info('keyword is invalid. Skip')
            return None
        keyword = task['keyword']
        num_urls = int(task['num_urls'])
        image_urls = self.fetch_image_urls(keyword, num_urls)
        unique_filename = str(keyword) + '.txt'
        unique_filename = cogtu_misc.escape_space(cogtu_misc.escape_fwd_slash(unique_filename))
        self.write_data_to_file(image_urls, unique_filename)
        return unique_filename


def test_work():
    KIUWorker = KeywordImageUrlWorker('google')
    keyword_numurls_testsets = {
        '范冰冰': 101,
        'Johnny Depp': 12
    }
    for keyword, num_urls in keyword_numurls_testsets.items():
        input_dict = {
            'keyword': keyword,
            'num_urls': num_urls
        }
        output_file_name = KIUWorker.work(input_dict)
        num_lines = sum(1 for line in open(output_file_name, 'r'))
        assert num_lines == num_urls
    print('TEST OK')

def test_get_num_pages_from_num_urls():
    KIUWorker = KeywordImageUrlWorker('google')
    assert KIUWorker.get_num_pages_from_num_urls(100) == 1
    assert KIUWorker.get_num_pages_from_num_urls(15) == 1
    assert KIUWorker.get_num_pages_from_num_urls(0) == 0
    assert KIUWorker.get_num_pages_from_num_urls(200) == 2
    assert KIUWorker.get_num_pages_from_num_urls(201) == 3
    print('TEST OK')

# test_work()
# test_get_num_pages_from_num_urls()
