#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-

from GoogleScraper import scrape_with_config, GoogleSearchError
from worker_fetch import FetchWorker
import hashlib
import logging
import sys
import ujson
import random
from time import sleep
sys.path.append('..')
import cogtu_misc
from collections import defaultdict

RETRY = 3

class KeywordFieldsWorker(FetchWorker):

    def __init__(self, search_engines):
        FetchWorker.__init__(self)
        self.gscraper_config = {
            'SCRAPING': {
                'search_engines': search_engines,
                'search_type': 'normal',
                'scrape_method': 'http',
                'num_pages_for_keyword': 1
            },
            'GLOBAL': {
                'do_caching': 'False'
            }
        }

    def fetch_info(self, keyword):
        self.gscraper_config['SCRAPING']['keyword'] = keyword
        info = defaultdict(dict)
        info['num_results_for_query']['baidu'] = 0
        info['num_results_for_query']['google'] = 0
        for i in range(0, RETRY):
            try:
                search = scrape_with_config(self.gscraper_config)
            except GoogleSearchError as e:
                logging.info(e)
                search = ''
                return
            for serp in search.serps:
                text = serp.num_results_for_query
                if 'baidu' in serp.search_engine_name:
                    info['num_results_for_query']['baidu'] = int(cogtu_misc.get_first_number_from_text(text))
                elif 'google' in serp.search_engine_name:
                    info['num_results_for_query']['google'] = int(cogtu_misc.get_first_number_from_text(text))
            if info['num_results_for_query']['baidu'] is not 0 or\
                    info['num_results_for_query']['google'] is not 0:
                break
            logging.info('RETRYING...')
        return info

    def write_data_to_file(self, data, filename):
        with open(filename, 'w') as f:
            f.write(ujson.dumps(data))

    def work(self, keyword):
        if not keyword:
            logging.info('keyword is invalid. Skip')
            return None
        # s = random.random() * 10.0
        # logging.info('SLEEPING FOR ' + str(s) + ' SECONDS')
        # sleep(s)
        # logging.info('SLEPT')
        info = self.fetch_info(keyword)
        unique_filename = str(keyword) + '.txt'
        unique_filename = cogtu_misc.escape_space(cogtu_misc.escape_fwd_slash(unique_filename))
        self.write_data_to_file(info, unique_filename)
        return unique_filename


def test_work():
    KFWorker = KeywordFieldsWorker('baidu,google')
    kws = [u'范冰冰', u'王心凌']
    for kw in kws:
        KFWorker.work(kw)

# test_work()
