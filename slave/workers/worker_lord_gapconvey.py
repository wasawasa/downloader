from worker_lord import WorkerLord
import logging

class GapConveyWorkerLord(WorkerLord):
    def __init__(self, fetch_worker, convey_worker, s3_bucket, s3_folder, num_tasks_packup):
        WorkerLord.__init__(self, s3_bucket, s3_folder)
        self.num_tasks_packup = num_tasks_packup
        self.convey_worker = convey_worker
        self.fetch_worker = fetch_worker

    def run(self, task):
        unique_str = task['url']
        if self.fetch_worker.get_save_path() is None:
            folder_name = self.generate_unique_folder(unique_str)
            self.fetch_worker.set_save_path(folder_name)
            self.convey_worker.set_convey_path(folder_name)
        fname = self.fetch_worker.work(task)
        if not fname:
            logging.info('filename is empty')
            return None
        else:
            # Increment the task counter. If the task counter reaches the
            #   <num_tasks_packup> assigned to worker_lord, call conver_worker
            #   to zip the directory generated above, and send the zip to s3,
            #   and delete the directory and zip.
            self.increment_task_counter()
            if self.task_counter is not 0 and \
                    self.task_counter % \
                    self.num_tasks_packup == 0:
                self.convey_worker.work(self.convey_worker.get_convey_path())
                self.fetch_worker.set_save_path(None)
                self.convey_worker.set_convey_path(None)
            return fname

    def close(self):
        self.convey_worker.work(self.convey_worker.get_convey_path())
        self.fetch_worker.close()
