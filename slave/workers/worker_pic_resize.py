#!/usr/bin/env python
# encoding: utf8
# -*- coding: encoding -*-

# Author: Nan Chen
# Date: Jun 9

import os
from scipy import misc
import numpy as np
import shutil
import zipfile
from worker_fetch import FetchWorker

NUM_RETRY_FETCH = 3

"""
Copied from resizer.py
"""
def resize_one(src_path, des_path):
    im = misc.imread(src_path)
    [h, w] = im.shape[:2]
    ratio = 255.0 / min(h, w)
    s = np.round(ratio * np.array([h, w])).astype(int)
    im_r = misc.imresize(im, s)
    misc.imsave(des_path, im_r)

def resize(src_dir, des_dir, name_list):
    if not os.path.isdir(des_dir):
        os.mkdir(des_dir)

    for fname in name_list:
        src_path = os.path.join(src_dir, fname)
        des_path = os.path.join(des_dir, fname)
        resize_one(src_path, des_path)
"""
End of copy
"""

class PicResizeWorker(FetchWorker):

    def _unpack_zip(self, zip_filename, dest_folder):
        if not zipfile.is_zipfile(zip_filename):
            return False
        with zipfile.ZipFile(zip_filename, 'r') as zip_file:
            zip_file.extractall(dest_folder)
        return True

    def write_data_to_file(self, data, filename):
        with open(filename, 'w') as f:
            f.write(data)

    def work(self):
        zip_filename = self.get_task_url().split('/')[-1]
        os.system('aws s3 cp ' + self.get_task_url() + ' ./')
        if not self._unpack_zip(zip_filename, self.get_save_path()):
            return None

        # Unzipping the file will generate a folder which contains a
        #   subfolder. The images we want are all in the subfolder, so
        #   the following code moves these images out of the subfolder.
        for folder_name in os.listdir(self.get_save_path()):
            if os.path.isdir(self.get_save_path() + '/' + folder_name):
                for filename in os.listdir(self.get_save_path() + '/' + folder_name):
                    shutil.copy(self.get_save_path() + '/' + folder_name + '/' + filename,
                                self.get_save_path() + '/' + filename)
                shutil.rmtree(self.get_save_path() + '/' + folder_name)

        # Move these images to a tmp folder. We will resize images in
        #   the tmp folder into self.get_save_path()
        os.mkdir('tmp_contain')
        for filename in os.listdir(self.get_save_path()):
            shutil.copy(self.get_save_path() + '/' + filename,
                        'tmp_contain/' + filename)
        resize(src_dir='tmp_contain', des_dir=self.get_save_path(),
                       name_list=os.listdir('tmp_contain'))
        shutil.rmtree('tmp_contain')

        return zip_filename
