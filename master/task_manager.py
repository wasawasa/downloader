import logging


class TaskManager:
    def __init__(self, task_file_name, task_counter_file_name):
        self.task_file_name = task_file_name
        self.task_counter_file_name = task_counter_file_name
        self.num_lines = sum(1 for line in open(self.task_file_name))
        self.task_file = open(self.task_file_name, 'r')
        self.task_counter = self.get_task_counter()
        logging.info('Starting from the ' + str(self.task_counter) + '-th task.')
        self.move_file_ptr_to_line(num_lines=self.task_counter)

    def get_task_counter(self):
        with open(self.task_counter_file_name, 'r') as f:
            line = f.readline()
            if line == '':
                return 0
            else:
                return int(line.strip())

    def move_file_ptr_to_line(self, num_lines):
        for i in range(0, num_lines):
            self.task_file.readline()

    def pop_task(self):
        task_string = self.task_file.readline()
        if task_string == '':
            print 'EOF'
            return None
        self.task_counter += 1
        with open(self.task_counter_file_name, 'w') as f:
            f.write(str(self.task_counter))
        task_string = self.process_url_to_json(task_string.strip())
        return task_string

    def stop(self):
        self.task_file.close()

    # TODO: this should be a private method in subclass
    def process_url_to_json(self, task_string):
        ret_val = dict()
        ret_val['task'] = task_string
        return ret_val

    def add_task(self, task):
        with open('../unfinished_tasks.txt', 'a') as f:
            f.write(task.strip() + '\n')
        pass

    def get_progress(self):
        return self.task_counter, self.num_lines


