import pika
import datetime


class ConnectionManager:
    def __init__(self, queue_name, durable=False, callback=None, no_ack=True):
        self.queue_name = queue_name
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(
            host='localhost'))
        self.channel = self.connection.channel()
        self.last_wake_time = datetime.datetime.utcnow()
        self.channel.queue_declare(queue_name, durable)
        self.channel.exchange_declare(exchange='broadcast',
                                      type='fanout')
        self.callback = None
        if callback is not None:
            self.callback = callback
            self.message_tag = self.channel.basic_consume(
                callback,
                queue=queue_name,
                no_ack=no_ack
            )

    def get_task_queue_size(self):
        queue = self.channel.queue_declare(self.queue_name, durable=True, passive=True)
        message_count = queue.method.message_count
        return message_count

    def stop(self):
        if not self.connection.is_closed:
            self.connection.close()

    def publish(self, message):
        self.channel.basic_publish(exchange='',
                                   routing_key=self.queue_name,
                                   body=message
                                   #properties=pika.BasicProperties(delivery_mode=2)
                                   )

    def start_accepting_message(self):
        self.channel.start_consuming()

    def broadcast_task(self, message):
        self.channel.basic_publish(exchange='broadcast',
                                   routing_key='',
                                   body=message)

class Test:
    def __init__(self):
        self.conn = None
        self.counter = 0
        print 'init'

    def main(self):
        self.conn = ConnectionManager('message_test', 'task_test', self.call_back)
        self.conn.start_accepting_message()

    def call_back(self, ch, method, properties, body):
        print 'callback {}'.format(self.conn.get_task_queue_size())

    def send(self):
        self.counter += 1
        self.conn.publish_task('task_{}'.format(self.counter))
        self.conn.publish_message('sending')

    def query(self):
        print 'query {}'.format(self.conn.get_task_queue_size())

if __name__ == '__main__':
    t = Test()

    import sys
    sys.path.append('../')
    from repeated_timer import RepeatedTimer
    rt = RepeatedTimer(.1, t.send)

    # rt2 = RepeatedTimer(.5, t.query)

    t.main()
