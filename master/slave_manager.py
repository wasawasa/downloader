import sys
import random
from collections import deque
import ujson
import datetime
import logging

sys.path.append('..')
from api_screen import ScreenController

class CycleList:
    def __init__(self, size):
        self.l = deque()
        self.size = size

    def append(self, x):
        self.l.append(x)
        if len(self.l) > self.size:
            self.l.popleft()

    def getlist(self):
        return list(self.l)


class SlaveManager:
    def __init__(self, master_ip, slaves_ip, ssh_key,
                 s3_bucket, s3_folder, slave_buffer_size,
                 slave_num_every_packup,
                 slave_python_version,
                 slave_max_sec_each_task,
                 slave_awake_frequency):
        self.master_ip = master_ip
        self.slaves_ip = slaves_ip
        self.ssh_key = ssh_key
        self.s3_bucket = s3_bucket
        self.s3_folder = s3_folder
        self.slave_num_every_packup = slave_num_every_packup
        self.slave_max_sec_each_task = slave_max_sec_each_task
        self.slave_python_version = slave_python_version
        self.slave_awake_frequency = slave_awake_frequency
        self.slave_buffer_size = slave_buffer_size
        self.slave_list = []
        self.slave_dict = {}

    def make_slave(self, slave_info):

        slave_info['screen'] = '{}-{}-{}'.format(slave_info['host'], slave_info['screen'], random.random())
        slave_info['last_response'] = None
        slave_info['wait'] = CycleList(self.slave_buffer_size)
        return slave_info

    def add_slave(self, slave_info):
        """Make a dict containing concise info of a slave and record this dict.

        :param slave_info: dict. Basic info of a slave. Contains a field:
                 'host': str. Private ip (ipv4) address of slave machine
        :return: dict. contains the following fields:
                 'screen': str. IP address and a random number, connected by a '-'.
                           This will be used as the screen name on the remote machine
                 'last_response': None. This will be recording machine's last response
                 'wait': CycleList.
        """
        new_slave = self.make_slave(slave_info)
        self.slave_list.append(new_slave)
        self.slave_dict[new_slave['screen']] = new_slave
        return new_slave

    def run_slave(self, slave_info):
        """Start screen and run consumer script on remote machine.

        :param slave_info: dict. Contains fields:
                           'host': str. Private ip (ipv4) address of slave machine
                           'screen': str. IP address and a random number, connected by a '-'.
        :return: dict. Contains fields:
                 'controller': SlaveController. It should be running a script in remote screen.
        """
        logging.info('initializing slave', slave_info)
        slave_info['controller'] = ScreenController(slave_info['host'], 'ubuntu', self.ssh_key)
        logging.info('controller initialized')
        slave_info['controller'].execute_screen(
            self.start_command(master_ip=self.master_ip,
                               host=slave_info['host'],
                               screen=slave_info['screen'],
                               s3_bucket=self.s3_bucket,
                               s3_folder=self.s3_folder,
                               num_packup=self.slave_num_every_packup,
                               max_download_sec=self.slave_max_sec_each_task,
                               python_version=self.slave_python_version,
                               slave_awake_frequency=self.slave_awake_frequency),
            slave_info['screen']
        )
        return slave_info

    def kill_slave(self, slave_info):
        """Stop script running in remote machine.

        :param slave_info: dict. Contains fields:
                           'host': str. Private ip (ipv4) address of slave machine
                           'screen': str. IP address and a random number, connected by a '-'.
                           'controller': SlaveController.
        :return: dict. Contains fields:
                 'controller': SlaveController. It should not be running script remotely.
        """
        target_slave = self.slave_dict[slave_info['screen']]
        target_slave['controller'].kill_screen(target_slave['screen'])
        self.slave_list.remove(target_slave)
        self.slave_dict.pop(target_slave['screen'])
        return slave_info

    def get_num_slaves(self):
        """Return number of existing slaves."""
        return len(self.slave_list)

    def update_last_response(self, slave_info):
        """Update last response time of slave.

        :param slave_info: dict. Contains fields:
                           'host': str. Private ip (ipv4) address of slave machine
        :return dict. Contains fields:
                'last_response': datetime. The current time of date.
        """
        if slave_info['screen'] not in self.slave_dict:
            logging.error('screen name {} not found'.format(slave_info['screen']))
        self.slave_dict[slave_info['screen']]['last_response'] = datetime.datetime.utcnow()
        return self.slave_dict[slave_info['screen']]

    def stop(self, mode='brutally'):
        """Stop all script and screens running in all slaves."""
        # to_be_killed = [slave for slave in self.slave_list]
        # for slave in to_be_killed:
        #     self.kill_slave(slave)
        # return to_be_killed
        self.slave_dict.clear()
        self.slave_list = []


    def get_failed_slaves(self, master_wait_time):
        """Get info of all slaves whose running time exceeds master's waiting time.

        :param master_wait_time: int. Master's maximum time in seconds for waiting a slave to respond
        :return: list. Contains dicts that contain failed slave info.
        """
        failed_slaves = []
        for slave in self.slave_list:
            if slave['last_response'] is None:
                continue
            elif slave['last_response'] + datetime.timedelta(seconds=master_wait_time) < \
                    datetime.datetime.utcnow():
                logging.info('Failed slave: last_response = ' + str(slave['last_response']) + ', now is ' + str(datetime.datetime.utcnow()) + ', wait time is ' +str(master_wait_time))
                failed_slaves.append(slave)
        return failed_slaves

    def exist_slave(self, slave_info):
        """Decide if specified slave exists

        :param slave_info: dict. Contains fields:
                           'host': str. Private ip (ipv4) address of slave machine
        :return: bool. Whether a slave with specified host exists in record.
        """
        if type(slave_info) is str or type(slave_info) is unicode:
            slave_info = ujson.loads(slave_info)
        return slave_info['host'] in self.slave_dict

    # @staticmethod
    # def convert_json_to_dict(target):
    #     if type(target) == 'dict':
    #         return target
    #     while type(target) == 'str' or type(target) == 'unicode':
    #         target = ujson.loads(target)
    #     return target

    # TODO: @abc.abstract
    def start_command(self, master_ip, host, screen, s3_bucket, s3_folder, num_packup,
                      max_download_sec, python_version, slave_awake_frequency):
        return 'cd cogtu_crawler/slave/ && ' + python_version + ' slave.py ' + master_ip +\
               ' ' + host + ' ' + screen + ' ' + s3_bucket + ' ' + s3_folder +\
               ' ' + num_packup + ' ' + max_download_sec + ' ' + slave_awake_frequency
