#!/usr/bin/env bash
sudo apt-get install rabbitmq-server -y
nohup sudo rabbitmq-server
sudo rm -r /home/ubuntu/cogtu_crawler
mkdir cogtu_crawler
unzip cogtu_crawler.zip -d cogtu_crawler
cd cogtu_crawler
cat aws_conf_input | aws configure
sudo chmod 777 /usr/local/lib/python3.4/dist-packages/GoogleScraper/parsing.py
sudo cp auxiliary/parsing.py /usr/local/lib/python3.4/dist-packages/GoogleScraper/parsing.py
sudo cp auxiliary/selenium_mode.py /usr/local/lib/python3.4/dist-packages/GoogleScraper/selenium_mode.py
