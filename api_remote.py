import os
import subprocess


class RemoteController:

    def __init__(self):
        pass

    @staticmethod
    def scp(ssh_key, ip, local_file_path, remote_file_path, use_subprocess=True):
        if use_subprocess:
            return subprocess.Popen(["scp -o StrictHostKeyChecking=no -i " + ssh_key + " " +
                                    local_file_path + " ubuntu@" + ip + ":" + remote_file_path], shell=True)
        else:
            return os.system("scp -o StrictHostKeyChecking=no -i " + ssh_key + " " +
                             local_file_path + " ubuntu@" + ip + ":" + remote_file_path)

    @staticmethod
    def scp_files(ssh_key, ip, path_pair_list, use_subprocess=True):
        for path_pair in path_pair_list:
            local_path = path_pair[0]
            remote_path = path_pair[1]
            RemoteController.scp(ssh_key, ip, local_path, remote_path, use_subprocess)

    @staticmethod
    def scp_to_ips(ssh_key, ip_list, local_file_path, remote_file_path, use_subprocess=True, communicate=False):
        subprocess_list = list()
        for ip in ip_list:
            p = RemoteController.scp(ssh_key, ip, local_file_path, remote_file_path, use_subprocess)
            subprocess_list.append(p)
        if use_subprocess and communicate:
            for p in subprocess_list:
                p.communicate()

    @staticmethod
    def remote_run(ssh_key, ip, cmd, use_subprocess=True):
        print "ssh -o StrictHostKeyChecking=no -i " +\
                              ssh_key + " ubuntu@" + ip + " " + cmd
        if use_subprocess:
            subprocess.Popen(["ssh -o StrictHostKeyChecking=no -i " +
                              ssh_key + " ubuntu@" + ip + " " + cmd], shell=True)
        else:
            os.system("ssh -o StrictHostKeyChecking=no -i " +
                      ssh_key + " ubuntu@" + ip + " " + cmd)
