__author__ = 'zehua'

from paramiko.client import SSHClient
import paramiko
import logging

def cmd_start_screen(screen_name):
    return "screen -AdmS " + screen_name

def cmd_detach_screen(screen_name):
    return "screen -d " + screen_name

def cmd_execute_screen(cmd, screen_name, output_file=None):
    postfix = "\\n'"
    if output_file:
        postfix = " > " + output_file + "\\n'"
    return "screen -SL " + screen_name + " -X stuff $'" + cmd + postfix

def cmd_kill_screen(screen_name):
    return 'screen -S ' + screen_name + ' -X quit'

def ssh_execute(client, cmd):
    _, std_out, _ = client.exec_command(cmd, get_pty=True)
    return std_out.readlines()

def ssh_client(server_ip, server_user, server_key):
    client = SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.load_system_host_keys()
    client.connect(server_ip, username=server_user, key_filename=server_key)
    return client

class ScreenController:

    def __init__(self, server_ip, server_user, server_key):
        self.client = ssh_client(
            server_ip=server_ip, server_user=server_user, server_key=server_key)
        self.client.set_log_channel('ssh_control')

    def close(self):
        self.client.close()

    def start_screen_with_cmd(self, cmd, screen_name):
        std_out_stream = []
        _, std_out, _ = self.client.exec_command(
            cmd_start_screen(screen_name))
        std_out_stream.extend(std_out.readlines())

        _, std_out, _ = self.client.exec_command(
            cmd_execute_screen(cmd, screen_name),
            get_pty=True)
        std_out_stream.extend(std_out.readlines())
        return std_out_stream

    def execute(self, cmd):
        _, std_out, _ = self.client.exec_command(cmd)
        return std_out.readlines()

    def execute_no_wait(self, cmd):
        self.client.exec_command(cmd + ' > /dev/null 2>&1 &')

    def execute_screen(self, cmd, screen_name):
        self.execute_no_wait('screen -dmS ' + screen_name + ' sh')
        self.execute_no_wait('screen -S ' + screen_name + ' -X stuff "' + cmd + '\n"')

    def kill_screen(self, screen_name):
        logging.info('killing screen with cmd: ' + 'screen -S ' + screen_name + ' -X quit')
        self.execute('screen -S ' + screen_name + ' -X quit')

    # def kill_screen(self, screen_name):
    #     _, std_out, _ = self.client.exec_command(
    #         cmd_kill_screen(screen_name),
    #         get_pty=True)
    #     print 'screen killed'



# # example
# s = SlaveController('52.2.242.184', 'ubuntu', '/home/zehua/.ssh/downloader.pem')
# s.execute_screen('python test/run.py', 'screen_id_123')
# s.kill_screen('screen_id_123')
# s.close()
