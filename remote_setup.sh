#!/usr/bin/env bash
# This script should run on remote machine in set up envirenment.case
sudo apt-get install unzip
sudo apt-get update -y
sudo apt-get install python-pip -y
sudo easy_install3 pip
sudo apt-get install python3-setuptools
sudo pip install pika
sudo pip3 install python3-pika
sudo apt-get install libxml2-dev libxslt-dev python-dev -y
sudo apt-get install python-libxml2 -y
sudo apt-get install python-libxslt1 -y
sudo apt-get install python3-dev -y
sudo pip install ujson
sudo pip install mechanize
sudo apt-get install python-lxml
sudo pip install awscli
sudo pip install mongoengine
sudo pip install paramiko
sudo apt-get install sshpass
sudo pip install virtualenv
sudo apt-get install xvfb -y
sudo pip install virtualenvwrapper
sudo apt-get install zlibc zlib1g zlib1g-dev -y
sudo pip install GoogleScraper
sudo apt-get -f install -y
sudo pip install pyvirtualdisplay
sudo apt-get install chromium-browser -y
wget http://chromedriver.storage.googleapis.com/2.12/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
rm chromedriver_linux64.zip
sudo mv chromedriver /usr/bin/
sudo chmod a+x /usr/bin/chromedriver
sudo pip2 install selenium
sudo pip2 install pyvirtualdisplay
sudo pip2 install beautifulsoup4
