import pika
import sys
import json
import ConfigParser
import subprocess
import os
import logging

class Herald:
    def __init__(self):
        self.connection = None
        self.channel = None
        self.cmd_map = {
            'USAGE': self.usage,
            'STOP': self.stop,
            'ADD_SLAVE_ALL': self.add_slave_all,
            'KILL_SLAVE_ALL': self.kill_all_remote_screen,
            'START': self.start,
            'DEPLOY': self.deploy,
            'RUN_CMD_REMOTE': self.run_cmd_remote,
            'COPY_REMOTE': self.copy_files_to_remote,
            'UPDATE_IP': self.update_config_ip
        }

    def get_connection(self):
        if self.connection is None or self.connection.is_closed:
            self.connection = pika.BlockingConnection(pika.ConnectionParameters(
                host='localhost'))
            self.channel = self.connection.channel()
            self.channel.queue_declare(queue='message')
        return self.connection, self.channel

    def close_connection(self):
        if self.connection is None or self.connection.is_closed:
            return
        self.connection.close()

    def stop(self, info):
        connection, channel = self.get_connection()
        body = 'STOP {}'
        channel.basic_publish(exchange='',
                              routing_key='message',
                              body=body)
        self.close_connection()

    def send_message_to_master(self, message):
        connection, channel = self.get_connection()
        channel.basic_publish(exchange='',
                              routing_key='message',
                              body=message)
        self.close_connection()

    def scp(self, key, ip, file_name):
        subprocess.Popen(["scp -o StrictHostKeyChecking=no -i " + key + " " + file_name + " ubuntu@" + ip + ":~/cogtu_crawler/" + file_name], shell=True)

    def scp_files(self, key, ip, file_names):
        for file_name in file_names:
            self.scp(key, ip, file_name)

    def restart_slave(self, info):
        pass

    def stat(self, info):
        pass

    def start(self, info):
        self.send_message_to_master('START {}')

    def configure(self, info):
        self.send_message_to_master('RECONFIGURE {}')

    def kill_slave(self, slave_host):
        self.send_message_to_master('KILL_SLAVE ' + json.dumps({"host": slave_host}))

    def add_slave(self, host, queue_id):
        self.send_message_to_master('ADD_SLAVE ' + json.dumps({"host": host, "screen": queue_id}))

    def add_slave_all(self, info):
        config = ConfigParser.ConfigParser(allow_no_value=True)
        config.read('config.conf')
        num_screen_each_machine = int(config.get('deployment', 'num_screen_each_machine'))
        for ip in config.get('main', 'slaves_private_ip').split(','):
            for queue_id in range(0, int(num_screen_each_machine)):
                self.add_slave(ip, queue_id)

    def kill_all_remote_screen(self, info):
        config = ConfigParser.ConfigParser(allow_no_value=True)
        config.read('config.conf')
        ssh_key = config.get('deployment', 'local_ssh_key')
        for ip in config.get('main', 'slaves_public_ip').split(','):
            subprocess.Popen(["ssh -o StrictHostKeyChecking=no -i " + ssh_key + " ubuntu@" + ip + " 'killall screen'"], shell=True)

    def update_config_ip(self, arg):
        import boto.ec2
        config = ConfigParser.ConfigParser(allow_no_value=True)
        config.read('config.conf')

        # Get machine tag specified on EC2 console
        master_ec2_tag = raw_input('Please enter the ec2 tag for master: ')
        slave_ec2_tag = raw_input('Please enter the ec2 tag for slaves: ')

        # Get public and private ip addresses of the master and the slaves with above tags
        conn = boto.ec2.connect_to_region("us-east-1")
        master_reservations = conn.get_all_instances(filters={"tag:Name": master_ec2_tag,  "instance-state-name": "running"})
        slave_reservations = conn.get_all_instances(filters={"tag:level_one" : slave_ec2_tag,  "instance-state-name": "running"})
        master_instance = [i for r in master_reservations for i in r.instances]
        slave_instances = [i for r in slave_reservations for i in r.instances]
        master_public_ip = [i.ip_address for i in master_instance][0]
        master_private_ip = [i.private_ip_address for i in master_instance][0]
        slaves_public_ip = [i.ip_address for i in slave_instances]
        slaves_private_ip = [i.private_ip_address for i in slave_instances]
        conn.close()

        # Write private and public ip addresses to conf file
        ip_str = ""
        for ip in slaves_private_ip:
            ip_str += ip + ","
        ip_str = ip_str[:-1]
        config.set(section='main', option='master_private_ip', value=master_private_ip)
        config.set(section='main', option='slaves_private_ip', value=ip_str)

        ip_str = ""
        for ip in slaves_public_ip:
            ip_str += ip + ","
        ip_str = ip_str[:-1]
        config.set(section='main', option='master_public_ip', value=master_public_ip)
        config.set(section='main', option='slaves_public_ip', value=ip_str)

        with open('config.conf', 'w') as f:
            config.write(f)
        return master_public_ip, master_private_ip, slaves_public_ip, slaves_private_ip

    def deploy(self, arg):
        master_public_ip, master_private_ip, slaves_public_ip, slaves_private_ip = self.update_config_ip(arg)
        config = ConfigParser.ConfigParser(allow_no_value=True)
        config.read('config.conf')

        # Zip and copy code to the machines, and run deployment shell script
        setup_script = config.get('deployment', 'setup_script')
        deploy_script = config.get('deployment', 'deploy_script')
        local_ssh_key = config.get('deployment', 'local_ssh_key')
        os.system('rm ../cogtu_crawler.zip')
        os.system('rm ../cogtu_crawler.zip')
        os.system('zip -r ../cogtu_crawler.zip .')
        os.system("scp -o StrictHostKeyChecking=no -i " + local_ssh_key + " ../cogtu_crawler.zip ubuntu@" + master_public_ip + ":~/cogtu_crawler.zip")
        os.system("scp -o StrictHostKeyChecking=no -i " + local_ssh_key + " " + local_ssh_key + " ubuntu@" + master_public_ip + ":~/.ssh")
        if '--env' in arg:
            p = subprocess.Popen(["ssh -o StrictHostKeyChecking=no -i " + local_ssh_key + " ubuntu@" + master_public_ip + " 'bash -s' < " + setup_script], shell=True)
            p.communicate()
        subprocess.Popen(["ssh -o StrictHostKeyChecking=no -i " + local_ssh_key + " ubuntu@" + master_public_ip + " 'bash -s' < " + deploy_script], shell=True)

        procs = list()
        for slave_ip in slaves_public_ip:
            p = subprocess.Popen(['scp -o StrictHostKeyChecking=no -i ' + local_ssh_key + ' ../cogtu_crawler.zip ubuntu@' + slave_ip + ':~/'], shell=True)
            procs.append(p)
        for p in procs:
            p.communicate()

        if '--env' in arg:
            procs = list()
            for slave_ip in slaves_public_ip:
                p = subprocess.Popen(["ssh -o StrictHostKeyChecking=no -i " + local_ssh_key + " ubuntu@" + slave_ip + " 'bash -s' < " + setup_script], shell=True)
                procs.append(p)
        for p in procs:
            p.communicate()

        procs = list()
        for slave_ip in slaves_public_ip:
            p = subprocess.Popen(["ssh -o StrictHostKeyChecking=no -i " + local_ssh_key + " ubuntu@" + slave_ip + " 'bash -s' < " + deploy_script], shell=True)
            procs.append(p)

    def run_cmd_remote(self, cmd):
        config = ConfigParser.ConfigParser(allow_no_value=True)
        config.read('config.conf')
        ssh_key = config.get('deployment', 'local_ssh_key')
        slaves_host = config.get('main', 'slaves_public_ip').split(',')
        for slave_ip in slaves_host:
            subprocess.Popen(["ssh -o StrictHostKeyChecking=no -i " + ssh_key + " ubuntu@" + slave_ip + " '" + 'killall screen' + "'"], shell=True)

    def copy_files_to_remote(self, info):
        config = ConfigParser.ConfigParser(allow_no_value=True)
        config.read('config.conf')
        ssh_key = config.get('deployment', 'local_ssh_key')
        master_public_ip = config.get('main', 'master_public_ip')
        slaves_public_ip = config.get('main', 'slaves_public_ip').split(',')
        files_to_master = config.get('copy_remote', 'files_to_master').split(',')
        files_to_slave = config.get('copy_remote', 'files_to_slave').split(',')
        for slave_ip in slaves_public_ip:
            self.scp_files(ssh_key, slave_ip, files_to_slave)
        self.scp_files(ssh_key, master_public_ip, files_to_master)

    def usage(self, info):
        print 'python herald.py <argument>'
        print 'Argument list:'
        print '    USAGE:                   print usage info'
        print '    STOP:                    Stop master'
        print '    ADD_SLAVE_ALL:           Start all slaves whose ips are specified in config.conf'
        print '    KILL_SLAVE_ALL:          Stop all slaves (screens) controller by this master'
        print '    START:                   Start the mater. Usually this command is run after ADD_SLAVE_ALL'
        print '    DEPLOY <arg>:            Runs remote_deploy.sh'
        print '                             args:'
        print '                                 --env: Runs remote_setup.sh before running remote_deploy.sh.'
        print '    RUN_CMD_REMOTE <cmd>:    Run <cmd> as bash command on all slave hosts.'
        print '    COPY_REMOTE:             Copy files to remote master and slaves. Files are specified in "copy_remote" section.'
        print "    UPDATE_IP:               Update master's and slaves' public and private ips by reading user inputs."

if __name__ == '__main__':
    cmd = sys.argv[1]
    arg = None if len(sys.argv) is 2 else sys.argv[2]
    herald = Herald()
    herald.cmd_map[cmd](arg)
